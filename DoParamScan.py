import os,sys
from libtest_comm import test_comm
from libitk_ic_over_netio_next import ICNetioNextHandler
from collections import OrderedDict
from time import sleep


import os
import sys
import time
import signal
import PSUController
import PSURemoteController
import PSUConfig


import shutil
import json


def FEPower(enable):

    ConfigFileOnly_Opto="/home/itkpix/itk-felix-sw/PySerialComm/CRCard/161R.card"    
    config=PSUConfig.PSUConfig(ConfigFileOnly_Opto)
    psus=config.getPsus()
    controller=PSUController.PSUController(psus)
    
        #### Check internal Voltage Before Powering
    for psu in psus:
        if not "itkpix" in psu.lower(): continue
        VoltageSet=controller.getSetVoltage(psu)
        CurrentLimit=controller.getCurrentLimit(psu)

        ##Compare them for the defaults in the config file 10% error
        if not (float(psus[psu]["Vmax"])*1.1 > VoltageSet and float(psus[psu]["Vmax"])*0.9 < VoltageSet ):
            print("Error PSU's set voltage doesn't agree with the one set on Opto")
            exit(1)

    #### Turn on/off the Power
    for psu in psus:
        if not "itkpix" in psu.lower(): continue
        controller.setOutput(psu,enable)
        time.sleep(1) ##Sleep is need for the voltage values to update

        print("FE Power is",enable)


        VoltageOn=controller.getVoltage(psu)
        CurrentOn=controller.getCurrent(psu)
        
        if enable:
            if not (float(psus[psu]["Imax"])*1.1 > CurrentOn 
                    and float(psus[psu]["Imax"])*0.9 < CurrentOn ):
                    print("FE Power is differnet then expected, powering the optoBoard off")
                    controller.setOutput(psu,False)
                    exit(1)



def ChangeConf(OrigFEConf,FEConf,Register,Val):
    shutil.copyfile(OrigFEConf,FEConf)
    jsonFile=open(FEConf,"r+")
    data=json.load(jsonFile)
    data["RD53B"]["GlobalConfig"][Register]=Val
    jsonFile.write(json.dumps(data, indent=2),)
    jsonFile.close()




OrigFEConf=sys.argv[1]
FIDElinkTX=sys.argv[2]
FIDElinkRX=sys.argv[3]
FIDBus=sys.argv[4]
FEConf=os.getcwd()+"/ParamScan-Register.json"















FIDElinkRX= int(FIDElinkRX)
FIDElinkTX= int(FIDElinkTX)


#Extract the elinks and everything from the FID
ElinkTX = (FIDElinkTX>>16) & 0x7FFFF #19 Bits 
LinkIDTX = (FIDElinkTX>>21) & 0x3FFF # 14 Bits
LocalTX = (FIDElinkTX>>16) & 0x1F # 5Bits

ElinkRX = (FIDElinkRX>>16) & 0x7FFFF #19 Bits 
LinkIDRX = (FIDElinkRX>>21) & 0x3FFF # 14 Bits
LocalRX = (FIDElinkRX>>16) & 0x1F # 5Bits


#This can be changed to optoBoard G number
FIDOptoTX = 0x1000000000118000 + 0x400000*LinkIDTX
FIDOptoRX = 0x10000000001d0000 + 0x400000*LinkIDRX




#Get OptoBoard Configuration Parameters

GroupTX=int((ElinkTX%64)/4)
ChannelTX=int((ElinkTX%64)%4)
GroupRX=int((ElinkRX%64)/4)
ChannelRX=int(0)


print("-------------")
print("Will Run Scan on; FIDTX",FIDElinkTX,"->",LinkIDTX,ElinkTX,LocalTX,"FIDRX",FIDElinkRX,"->",LinkIDRX,ElinkRX,LocalRX)
print("Extracted Opto Info is TX;",FIDOptoTX,"RX",FIDOptoRX,"gTX",GroupTX,"cTX",ChannelTX,"gRX",GroupRX,"cRX",ChannelRX)
print("")




IC = ICNetioNextHandler("lo", FIDBus, FIDOptoTX, FIDOptoRX , False, 1, 0x74 )


for PolarityTX in [1,0]:
    for PolarityRX in [0,1]:

        FEPower(False);

        for itry in range(0,10):
            try:
                IC.sendRegs({"EPRX{:d}{:d}CHNCNTR_EPRX{:d}{:d}INVERT".format(GroupRX,ChannelRX,GroupRX,ChannelRX):[PolarityRX]})
                IC.sendRegs({"EPTX{:d}{:d}_{:d}{:d}CHNCNTR_EPTX{:d}{:d}INVERT".format(GroupTX,(ChannelTX+1),GroupTX,ChannelTX,GroupTX,ChannelTX):[PolarityTX]})
                sleep(0.5)
                break
            except:
                print("Send register failed, trying again")
                continue
        FEPower(True)
        print("Sleeping after power on")
        sleep(1)

        for SldoTrimA in range (0,11):
            
            ChangeConf(OrigFEConf,FEConf,"SldoTrimA",SldoTrimA)

            ##Re-Try 10 times
            bRes=False
            for itry in range(0,10):
                try:
                    Scan = test_comm(FIDElinkTX,FIDElinkRX, FIDBus, FEConf)        
                    bRes = Scan.CheckCommScans()
                    del Scan
                    break
                except:
                    print("Scan failed, trying again")
                    continue


            if(bRes):
                print("Found a working configuration, exiting")
                print("PolarityTX:",PolarityTX,"PolarityRX:",PolarityRX)
                break



            sleep(1)
            #print("Testing TX:",PolarityTX,"RX:",PolarityRX,"Result:",bRes)
            print()
            print()
